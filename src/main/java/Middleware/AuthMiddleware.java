package Middleware;

import Framework.Exception.Http.AuthException;
import Framework.Init;
import Utlity.Token;
import spark.Request;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ashrith on 3/2/17.
 */
public class AuthMiddleware {

    String jwtKey,encryptkey;

    public AuthMiddleware(Init boostrap) {
        jwtKey = ((Map<String,Object>)boostrap.config.getSystemConfiguration().get("jwtappkey")).get("value").toString();
        encryptkey = ((Map<String,Object>)boostrap.config.getSystemConfiguration().get("encryptkey")).get("value").toString();
    }

    public  void authTokenCheck(Request req) throws AuthException {
        String token = req.headers("Authorization");
        Token tokenObj = new Token(jwtKey,encryptkey);
        HashMap<String, Object> tokenObject = tokenObj.parseToken(token);
        if (!tokenObject.get("subject").equals("authtoken")) {
            throw new AuthException("Sorry,not a valid  authorization token", "292");
        }
        req.attribute("id", tokenObject.get("id"));
        req.attribute("role", tokenObject.get("role"));
    }
}
