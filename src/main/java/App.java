
import Controllers.LinksController;
import Framework.Exception.ConfigurationException;
import Framework.Exception.DaoException;
import Framework.Exception.Http.*;
import Framework.Exception.JsonreaderException;
import Framework.Exception.ValidationException;
import Framework.Init;
import Framework.ORM.Adapter.Hbase;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.ORM.OutputProcessor;
import Middleware.AuthMiddleware;
import Utlity.Token;

import java.util.Map;

import static spark.Spark.*;
/**
 * Created by ashrith on 22/1/17.
 */

public class App {
    public static AdapterImplementation dataAdapter;
    public static OutputProcessor processor;
    public static String jwtKey,encryptkey;
    public static Utlity.Token tk = null;
    public static Framework.Init bootstrap = null;

    public static void main(String[] args) throws DaoException, ConfigurationException, JsonreaderException {

        if(args.length != 0 && !args[0].isEmpty()){
            bootstrap = new Framework.Init(args[0]);
        }else{
            bootstrap = new Framework.Init(System.getProperty("user.dir") + "/conf/applicationConfig.json");
        }

        dataAdapter = bootstrap.getDao();
        processor = bootstrap.getOutputProcessor();
        jwtKey = ((Map<String,Object>) bootstrap.config.getSystemConfiguration().get("jwtappkey")).get("value").toString();
        encryptkey = ((Map<String,Object>)bootstrap.config.getSystemConfiguration().get("encryptkey")).get("value").toString();
        tk = new Token(jwtKey,encryptkey);
        sparkConfig(bootstrap);
        Controllers();
        exceptions();

    }

    private static void Controllers() {
        AuthMiddleware am = new AuthMiddleware(bootstrap);

        options("*",((request, response) -> {
            return "OK";
        }));

        post("/link",((request, response) -> {
            am.authTokenCheck(request);
            return LinksController.createLink(request,response);

        }));

        put("/link/:id",((request, response) -> {
            am.authTokenCheck(request);
            return LinksController.updateLink(request,response);

        }));

        put("/link/:id/status",((request, response) -> {
            am.authTokenCheck(request);
            return LinksController.updateLinkStatus(request,response);

        }));

        delete("/link/:id",((request, response) -> {
            am.authTokenCheck(request);
            return LinksController.deleteLink(request,response);

        }));

        get("/links",((request, response) -> {
            am.authTokenCheck(request);
            return LinksController.getUserLinks(request,response);
        }));

        awaitInitialization();
    }

    private static void sparkConfig(Init bootstrap) {
        port(Integer.parseInt(bootstrap.config.getPort()));

        notFound((req, res) -> {
            res.type("application/json");
            res.status(404);
            return "{\"message\":\"page not found\",\"status\":\"0\",\"errorcode\":\"404\"}";
        });

        internalServerError((req, res) -> {
            res.type("application/json");
            res.status(500);
            return "{\"message\":\"internal server error\",\"status\":\"0\",\"errorcode\":\"500\"}";
        });

        before((request, response) -> {
            String header = request.contentType();
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            response.header("Access-Control-Allow-Origin", "*");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            if (request.requestMethod().equals("POST") || request.requestMethod().equals("PUT")) {
                if ((header == null) || (!header.equals("application/json"))) {
                    throw new AuthException("Content-Type must be application/json", "29");
                }
            }
        });


        after((request, response) -> {
            if(!request.requestMethod().equals("OPTIONS")){
                response.type("application/json");
                bootstrap.sparkLogWriter(request,response);
            }
        });



    }

    private static void exceptions() {
        exception(EmptyPostRequest.class, (exception, req, res) -> ExceptionHandler.emptyRequest(exception, req, res));
        exception(MissingRequiredParameter.class,(exception, req, res)-> ExceptionHandler.requiresParam(exception,req,res));
        exception(ValidationError.class,(exception, req, res)-> ExceptionHandler.validationError(exception,req,res));
        exception(AuthException.class, (exception, req, res) -> ExceptionHandler.authError(exception, req, res));
        exception(DaoException.class, (exception, req, res) -> ExceptionHandler.daoException(exception, req, res));
        exception(ValidationException.class, (exception, req, res) -> ExceptionHandler.daoException(exception, req, res));
        exception(Exception.class, (exception, req, res) -> ExceptionHandler.internalServerError(exception, req, res));
    }

}

