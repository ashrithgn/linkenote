package Controllers;

import Framework.Exception.DaoException;
import Framework.Exception.Http.EmptyPostRequest;
import Framework.Exception.Http.ValidationError;
import Framework.Exception.ValidationException;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import spark.Request;
import spark.Response;
import java.util.*;

import static Framework.Init.dao;
import static Framework.Init.processor;
import static java.lang.Math.round;


/**
 * Created by ashrith on 22/1/17.
 */
public class LinksController {

    public static String createLink(Request req, Response res) throws DaoException {
        String userid = req.attribute("id");
        Map<String,String> params = new Gson().fromJson(req.body(), HashMap.class);
        params.put("userid",userid);
        params.put("processed","0");
        if(!params.containsKey("status")){
            params.put("status","toread");
        }
        int time = round(System.currentTimeMillis() / 1000);
        params.put("created",time+"");
        Map<String,Object> links = new HashMap<>();
        links.put("links",params);
        String id = UUID.randomUUID().toString();
        dao.add(links, id);
        links.put("id",id);
        updateUrlMeta(links);
        Map<String,Object> retval = new HashMap<>();
        retval.put("data",links);
        retval.put("status","1");
        retval.put("message","data added successfully");
        return new Gson().toJson(retval);
    }

    public static String deleteLink(Request req,Response res) throws DaoException {
        String id = req.params("id");
        String userid = req.attribute("id");
        dao.Delete(id,"links#userid");
        dao.Delete(id,null);
        dao.Delete(id,"links");
        Map<String,Object> retval = new HashMap<String,Object>();
        retval.put("data","");
        retval.put("status","1");
        retval.put("message","data deleted successfully");
        return new Gson().toJson(retval);
    }

    public static String updateLink(Request req,Response res) throws ValidationException, DaoException {
        String id = req.params("id");
        String userid = req.attribute("id");
        Map<String,Object> params = null;
        try{
            params = new Gson().fromJson(req.body(), HashMap.class);
            params.put("processed","0");
            if(!params.containsKey("status")){
                params.put("status","toread");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(id == null || id.isEmpty()){
            throw new ValidationException("id cannot be empty",1);
        }
        Map<String,Object> links = new HashMap<>();
        links.put("links",params);

        dao.update(links, id);
        links.put("id",id);
        updateUrlMeta(links);
        Map<String,Object> retval = new HashMap<>();
        retval.put("data",links);
        retval.put("status","1");
        retval.put("message","data update successfully");
        return new Gson().toJson(retval);
    }

    public static String getUserLinks(Request request, Response response) throws DaoException {
        String userid = request.attribute("id");
        List<Map> searchObj = dao.search("links#userid",userid,1,0);
        List<Map<String,Object>> data = new ArrayList<>();
        for (Map<String,Object> obj:searchObj) {
            Map<String,Object> link = new HashMap<>();
            link.put("id",obj.get("key").toString());
            Map<String,Object> innerlinks = new HashMap<>();
            innerlinks.put("tags","default");
            innerlinks.put("title","default");
            innerlinks.put("description","Description is not available");
            innerlinks.put("readtime","Read time is not available");
            Map<String,Object> tmp = (Map<String, Object>) processor.process(dao.getbyId(obj.get("key").toString(),null,1,0)).get("links");
            if(tmp.containsKey("foldername")){
                innerlinks.put("foldername",tmp.get("foldername").toString());
            }
            if(tmp.containsKey("url")){
                innerlinks.put("url",tmp.get("url").toString());
            }
            if(tmp.containsKey("tags")){
                innerlinks.put("tags",tmp.get("tags").toString());
            }
            if(tmp.containsKey("title")){
                innerlinks.put("title",tmp.get("title").toString());
            }
            if(tmp.containsKey("description")){
                innerlinks.put("description",tmp.get("description").toString());
            }
            if(tmp.containsKey("readtime")){
                innerlinks.put("readtime",tmp.get("readtime").toString());
            }
            if(tmp.containsKey("status")){
                innerlinks.put("status",tmp.get("status").toString());
            }
            if(tmp.containsKey("image")){
                innerlinks.put("image",tmp.get("image").toString());
            }
            if(tmp.containsKey("created")){
                innerlinks.put("created",tmp.get("created").toString());
            }
            link.put("links",innerlinks);
            Comparator<Map<String, Object>> mapComparator = new Comparator<Map<String, Object>>() {
                public int compare(Map<String, Object> m1, Map<String, Object> m2) {
                    String m1created = ((Map<String,Object>)m1.get("links")).get("created").toString();
                    String m2created = ((Map<String,Object>)m2.get("links")).get("created").toString();
                    return m1created.compareTo(m2created);
                }
            };
            data.add(link);
            Collections.sort(data,mapComparator);
            Collections.reverse(data);
        }
        Map<String,Object> retval = new HashMap<>();
        retval.put("data",data);
        retval.put("count",data.size());
        retval.put("status","1");
        retval.put("message","data retrieved successfully");
        return new Gson().toJson(retval);
    }

    private static void updateUrlMeta(Map<String,Object> object){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String id = object.get("id").toString();
                Map<String,Object> linkObject = (Map<String,Object>)object.get("links");
                try{
                    Document doc = Jsoup.connect(linkObject.get("url").toString()).get();
                    String keywords = null;
                    try{keywords = doc.select("meta[name=keywords]").attr("content");}catch (Exception e){}
                    String description = null;
                    try{description = doc.select("head").select("meta[name=description]").attr("content");}catch (Exception e){}
                    String title = null;
                    try{title = doc.title();}catch (Exception e){}
                    String image = "default.png";
                    try{
                        if(!doc.body().select("img").first().attr("src").isEmpty()){
                            image = doc.body().select("img").first().attr("src");
                        }}catch (Exception e){}

                    if(!linkObject.containsKey("title")){
                        if(title != null && !title.isEmpty()){
                            linkObject.put("title",title);
                        }
                    }
                    linkObject.put("image",image);
                    if(!linkObject.containsKey("description")){
                        if(description != null && !description.isEmpty()){
                        linkObject.put("description",description);}
                    }
                    if(!linkObject.containsKey("keywords")){
                        if(keywords != null && !keywords.isEmpty()){
                            linkObject.put("tags",keywords.split(","));
                        }
                    }
                    String data[] = doc.body().text().split(" ");
                    Long readtime = round((data.length * 0.33)/60);
                    linkObject.put("readtime",readtime.toString() + " minutes approximate");
                    linkObject.put("processed","1");
                    object.put("links",linkObject);
                    if(object.containsKey("id")){
                        object.remove("id");
                    }
                    dao.update(object,id);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static Object updateLinkStatus(Request request, Response response) throws EmptyPostRequest, ValidationError, DaoException {
        String id = request.params("id");
        String userid = request.attribute("id");
        Map<String,Object> params = null;
        params = new Gson().fromJson(request.body(), HashMap.class);
        if(params.size() <= 0 ){
            throw new EmptyPostRequest("request cannot be empty","3");
        }

        if(!params.containsKey("status")){
            throw new ValidationError("status field is required","4");
        }
        String param = params.get("status").toString();
        params.clear();
        params.put("status",param);
        Map<String,Object> links = new HashMap<>();
        links.put("links",params);
        dao.update(links, id);
        Map<String,Object> retval = new HashMap<>();
        retval.put("data",links);
        retval.put("status","1");
        retval.put("message","data update successfully");
        return new Gson().toJson(retval);
    }
}

